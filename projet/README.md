## Projet Statistique

 **Auteurs** :
	- Mehdi KHADIR
	- Zohir YAMINI

 **Sujet : Accidents corporels de la circulation en France de 2005 à 2017**

Source :https://www.data.gouv.fr/fr/datasets/base-de-donnees-accidents-corporels-de-la-circulation/
From : Observatoire national interministériel de sécurité routière
