## Produits et services achetés sur internet

---

![Produits et services achetés sur internet](https://screenshotscdn.firefoxusercontent.com/images/726c23f3-a9d4-4335-80c2-8a274fd3251a.png)

Les données observées dans le diagramme en bâtons semblent cohérentes dans le sens où un type de produits ou services est associé a une valeur et une seul valeur numérique expiré en %.

Néanmoins, le total des % sur l'ensemble des produits et services achetés sur internet est égal à 543%. Il est par conséquence difficile de donnée une réel opinion objectif sur cette donnée statistique, c'est-à-dire, qu'il sera impossible de dire, si les e-acheteurs achètent plus de chaussures ou des produits culturels sur l'échantillon de la population qui a était prise comme référence.

L'échelle, semble correcte par rapport au pourcentage de chaque valeur, mais l'origine n'est pas précisée.
Les couleurs des bâtonnets ne sont pas différenciées, peut-être qu'il serait plus pertinent de pouvoir observer une palette de couleur croissante comme dans l'image suivante :

![Image of briot-jerome](https://briot-jerome.developpez.com/matlab/tutoriels/gestion-couleurs/images/allcolormaps_a.png)

---

Source de donnée :

https://www.fevad.com/wp-content/uploads/2018/06/Chiffres-Cles-2018.pdf [Page 4]